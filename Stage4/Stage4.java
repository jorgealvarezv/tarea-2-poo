import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Stage4 extends Application {
    public void start(Stage primaryStage) {
        //Lámpara 1:
        int lampChannel1=2;
        Cloud cloud = new Cloud();
        Lamp lamp1 = new Lamp(lampChannel1);
        cloud.addLamp(lamp1);
        //Lampara 2:
        int lampChannel2=3;
        Lamp lamp2 = new Lamp(lampChannel2);
        cloud.addLamp(lamp2);
        LampControl lampControl1 = new LampControl(lampChannel1, cloud);
        HBox hBox = new HBox(20);
        hBox.setPadding(new Insets(20));
        hBox.setAlignment(Pos.CENTER);
        hBox.getChildren().addAll(lamp1.getView(), lamp2.getView(), lampControl1.getView());
        BorderPane pane = new BorderPane();
        pane.setPadding(new Insets(20));
        pane.setBottom(hBox);
        //Roller 1:
        int shadeChannel1=1;
        RollerShade rs1 = new RollerShade(shadeChannel1, 2, 250, 140, "http://profesores.elo.utfsm.cl/~agv/elo329/1s22/Assignments/20220430_101027.mp4");
        cloud.addRollerShade(rs1);

        ShadeControl shadeControl = new ShadeControl(shadeChannel1,cloud);
        int shadeChannel2=2;
        RollerShade rs2 = new RollerShade(shadeChannel2, 3, 144, 140, "http://profesores.elo.utfsm.cl/~agv/elo329/1s22/Assignments/20220430_100849.mp4");
        cloud.addRollerShade(rs2);
        HBox rollerBox = new HBox(2);
        rollerBox.getChildren().addAll(rs1.getView(),rs2.getView());
        pane.setCenter(rollerBox);
        rollerBox.setPadding(new Insets(20));
        rollerBox.setAlignment(Pos.CENTER);
        hBox.getChildren().add(0,shadeControl.getView());
        Scene scene = new Scene(pane, 550, 450);
        scene.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if(keyEvent.getCode() == KeyCode.SPACE || keyEvent.getCode() == KeyCode.P){
                    lampControl1.pressPower();
                }
                else {
                    shadeControl.stop();
                }
            }
        });
        scene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent keyEvent) {
                if(keyEvent.getCode().isArrowKey()){
                    if(keyEvent.getCode() == KeyCode.UP){
                        shadeControl.startUp();
                        keyEvent.consume();
                    }
                    else if(keyEvent.getCode() == KeyCode.DOWN){
                        shadeControl.startDown();
                        keyEvent.consume();
                    }

                }
                /*if (keyEvent.getCode() == KeyCode.S) {
                }*/


            }
        });

        primaryStage.setTitle("Domotic Devices Simulator");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
