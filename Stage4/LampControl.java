import javafx.scene.layout.Pane;

public class LampControl {
    public LampControl(int channel, Cloud c){
        this.channel= channel;
        cloud = c;
        view = new LampControlView(this);
    }
    public void pressPower(){

        cloud.changeLampPowerState(channel);
    }
    public int getChannel(){
        return channel;
    }

    public void setChannel(int channel) {
        this.channel = channel;
    }
    public void change_RED(int red_value){
        cloud.changeRed(red_value,channel);
    }
    public void change_GREEN(int green_value){
        cloud.changeGreen(green_value,channel);
    }
    public void change_BLUE(int blue_value){
        cloud.changeBlue(blue_value,channel);
    }

    public Pane getView() { return view;}
    private int channel;
    private Cloud cloud;
    private Pane view;
}
