import javafx.scene.Group;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.media.MediaView;
import javafx.scene.paint.Color;
import javafx.scene.paint.PhongMaterial;
import javafx.scene.shape.Cylinder;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;


public class RollerShadeView extends Group {
    public RollerShadeView(double maxLength, double width, double length, double radius, Color color, String url) {
        Rectangle background = new Rectangle(5,5, width-10,maxLength-5);
        color = Color.ROSYBROWN;
        background.setFill(Color.SADDLEBROWN);  // I chose Blue
        getChildren().add(background);
        Media media = new Media(url);
        MediaPlayer mp = new MediaPlayer(media);
        MediaView mv = new MediaView(mp);
        mv.setX(6);
        mv.setY(6);
        mv.setFitHeight(maxLength-6);
        mv.setFitWidth(width-11);

        mp.setVolume(0);
        mp.setCycleCount(MediaPlayer.INDEFINITE);
        mp.play();
        getChildren().add(mv);




        cloth = new Rectangle (5, 5,width-10, length);//x, y, ancho, altura
        cloth.setFill(color);
        getChildren().add(cloth);

        // Rolled up shade cloth
        //Ellipse rightSide = new Ellipse(width, radius,radius/2,radius);
        //rightSide.setFill(color);
        //rightSide.setStroke(Color.BLACK);
        Cylinder cylinder = new Cylinder(100, 25.0f);
        cylinder.setHeight(width-10);
        cylinder.setTranslateX((width)/2);
        cylinder.setTranslateY(5);
        cylinder.setRotate(90);
        cylinder.setRadius(5);
        PhongMaterial material = new PhongMaterial();
        material.setDiffuseColor(Color.SADDLEBROWN);
        cylinder.setMaterial(material);


        //Cylinder ctm = new Cylinder(radius,width);

        getChildren().add(cylinder);
        //getChildren().add(rightSide);
        //¿?
    }
    public void setLength(double length) {
        cloth.setHeight(length);
    }
    private Rectangle cloth;
    private static String url_square;


}
