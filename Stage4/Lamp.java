import javafx.scene.Node;

public class Lamp extends DomoticDevice{
    public Lamp (int channel){
        super(channel);
        //this.channel= channel;
        r=g=b= 255;
        state = LampState.OFF;
        view = new LampView();
    }

    public void changePowerState(){

        state = state==LampState.ON ? LampState.OFF : LampState.ON;
        if (state==LampState.OFF) view.setColor((int)0,(int)0, (int)0);
        else view.setColor(r,g,b);
    }
    public void change_RED(int action){
        if (state==LampState.ON) {
            r = action;
            view.setColor(r, g, b);
        }


    }
    public void change_GREEN(int action){
        if (state==LampState.ON) {
            g = action;
            view.setColor(r, g, b);
        }


    }
    public void change_BLUE(int action){
        if (state==LampState.ON) {
            b = action;
            view.setColor(r, g, b);
        }

    }
    public Node getView() {
        return view;
    }
    //private int channel;
    private int r,g,b;
    private LampState state;
    private LampView view;
}
//ola k tal