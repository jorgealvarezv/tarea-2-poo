import com.sun.jdi.IntegerValue;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.Spinner;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class LampControlView extends VBox {
    public LampControlView(LampControl lampControl) {
        this.lampControl = lampControl;
        Image img = new Image("rsc/powerImage.jpg");
        ImageView view = new ImageView(img);
        Button bt = new Button();
        bt.setPrefSize(57,57);
        bt.setGraphic(view);
        HBox redSlider = new HBox(3);
        Label red_label = new Label("R");
        Label red_value = new Label();
        HBox greenSlider = new HBox(3);
        Label green_label = new Label("G");
        Label green_value = new Label();
        HBox blueSlider = new HBox(3);
        Label blue_label = new Label("B");
        Label blue_value = new Label();

        Spinner cha = new Spinner(2,3,2); //Canal 2 y 3 -> lamparas
        Slider red = new Slider(0,255,255);
        Slider green = new Slider(0,255,255);
        Slider blue = new Slider(0,255,255);


        cha.valueProperty().addListener(new ChangeListener() {
            @Override
            public void changed(ObservableValue observableValue, Object o, Object t1) {
                int valor = (int)cha.getValue();
                lampControl.setChannel(valor);
            }
        });
        red.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                int r = (int)red.getValue();

                lampControl.change_RED(r);
                red_value.setText("" + r);

            }
        });
        redSlider.getChildren().addAll(red_label, red, red_value);
        green.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                int g = (int)green.getValue();

                lampControl.change_GREEN(g);
                green_value.setText("" + g);

            }
        });
        greenSlider.getChildren().addAll(green_label, green, green_value);
        blue.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number t1) {
                int b = (int)blue.getValue();

                lampControl.change_BLUE(b);
                blue_value.setText("" + b);
            }
        });
        blueSlider.getChildren().addAll(blue_label, blue, blue_value);

        bt.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent press) {
                    int valor = (int)cha.getValue();

                    lampControl.setChannel(valor);
                    lampControl.pressPower();

                    }


            });

        setStyle("-fx-padding: 10;" +
                "-fx-border-style: solid inside;" +
                "-fx-border-width: 2;" +
                "-fx-border-radius: 10;" +
                "-fx-border-color: blue;"+
                "-fx-background-color:#E1FBCE;");
        getChildren().addAll(bt, cha, redSlider,greenSlider,blueSlider);

    }


    private LampControl lampControl;
}
