import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Control;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.*;
import javafx.scene.shape.Cylinder;
import javafx.scene.transform.Rotate;

public class ShadeControlView extends BorderPane {
    public ShadeControlView (ShadeControl sc){
        Image bUp = new Image("rsc/arriba.jpeg");
        Image bDown = new Image("rsc/abajo.jpeg");
        Image bLeft = new Image("rsc/izquierda.jpeg");
        Image bRight = new Image("rsc/derecha.jpeg");
        ImageView viewUp = new ImageView(bUp);
        ImageView viewDown = new ImageView(bDown);
        ImageView viewLeft = new ImageView(bLeft);
        ImageView viewRight = new ImageView(bRight);
        viewUp.setFitHeight(20);
        viewUp.setFitWidth(20);
        viewDown.setFitHeight(20);
        viewDown.setFitWidth(20);
        viewLeft.setFitHeight(20);
        viewLeft.setFitWidth(20);
        viewRight.setFitHeight(20);
        viewRight.setFitWidth(20);

        GridPane controlRS = new GridPane();
        ShadeControl shadeControl = sc;
        Button channelButton = new Button("  "+sc.getChannel()+ " ");
        Button leftButton = new Button();
        canal = sc.getChannel();


        leftButton.setGraphic(viewLeft);
        leftButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if(canal == 2){
                    canal = 1;
                    shadeControl.setChannel(canal);
                }
                channelButton.setText("  " + canal+" ");

            }
        });

        Button rightButton = new Button();
        rightButton.setGraphic(viewRight);
        rightButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                if(canal == 1){
                    canal = 2;
                    shadeControl.setChannel(canal);
                }
                channelButton.setText("  " + canal+" ");
            }
        });

        channelButton.setOnAction( e-> {
            shadeControl.setChannel(canal);
            sc.stop();
        });
        Button upButton = new Button();

        //upButton.setPrefSize(10,10);
        upButton.setGraphic(viewUp);
        upButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                shadeControl.setChannel(canal);
                sc.startUp();
            }
        });

        Button downButton = new Button();
        //downButton.setPrefSize(10,10);
        downButton.setGraphic(viewDown);
        downButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                shadeControl.setChannel(canal);
                sc.startDown();
            }
        });

        GridPane.setRowIndex(upButton,0);
        GridPane.setColumnIndex(upButton,1);
        GridPane.setRowIndex(leftButton,1);
        GridPane.setColumnIndex(leftButton,0);
        GridPane.setRowIndex(channelButton,1);
        GridPane.setColumnIndex(channelButton,1);
        GridPane.setRowIndex(rightButton,1);
        GridPane.setColumnIndex(rightButton,2);
        GridPane.setRowIndex(downButton,2);
        GridPane.setColumnIndex(downButton,1);
        controlRS.setMaxWidth(142);
        controlRS.setMinWidth(142);
        controlRS.setMaxHeight(110);
        controlRS.setMinHeight(110);
        controlRS.setStyle("-fx-padding: 10;" +
                "-fx-border-style: solid inside;" +
                "-fx-border-width: 2;" +
                "-fx-border-radius: 10;" +
                "-fx-border-color: blue;"+
                "-fx-background-color:#E1FBCE;");




        controlRS.getChildren().addAll(upButton,leftButton,channelButton,rightButton,downButton);

        setLeft(controlRS);
    }
    int canal;
}
