import javafx.scene.Group;
import javafx.scene.paint.Color;
import javafx.scene.shape.Polygon;

public class LampView extends Group {
    public LampView () {
        /** Creación de la base de la lámpara */
        Polygon base = new Polygon();
        base.getPoints().addAll(new Double[]{
                18d, 20d,
                18d, 50d,
                13d, 50d,
                10d, 53d,
                10d, 60d,
                30d, 60d,
                30d, 53d,
                27d, 50d,
                22d, 50d,
                22d, 20d});
        base.setFill(Color.GREEN);

        /** Creación de la pantalla de la lámpara */
        lampshade = new Polygon();
        lampshade.getPoints().addAll(new Double[]{
                -5d, 20d,
                45d, 20d,
                34d, 0d,
                4d, 0d

                });

        //lampshade.setFill(Color.rgb(0,0,0));//RGB
        lampshade.setStroke(Color.BEIGE);
        getChildren().addAll(base);
        getChildren().addAll(lampshade);
    }
    public void setColor(int r, int g, int b){
        lampshade.setFill(Color.rgb(r, g, b));
    }
    private Polygon lampshade;
}
