import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

public class Stage3 extends Application {
    public void start(Stage primaryStage) {
        //Lámpara 1:
        int lampChannel1=2;
        Cloud cloud = new Cloud();
        Lamp lamp1 = new Lamp(lampChannel1);
        cloud.addLamp(lamp1);
        //Lampara 2:
        int lampChannel2=3;
        Lamp lamp2 = new Lamp(lampChannel2);
        cloud.addLamp(lamp2);
        LampControl lampControl1 = new LampControl(lampChannel1, cloud);
        HBox hBox = new HBox(20);
        hBox.setPadding(new Insets(20));
        hBox.setAlignment(Pos.CENTER);
        hBox.getChildren().addAll(lamp1.getView(), lamp2.getView(), lampControl1.getView());
        BorderPane pane = new BorderPane();
        pane.setPadding(new Insets(20));
        pane.setBottom(hBox);
        //Roller 1:
        int shadeChannel1=2;
        RollerShade rs1 = new RollerShade(shadeChannel1, 2, 150, 100);
        cloud.addRollerShade(rs1);
        pane.setCenter(rs1.getView());
        ShadeControl shadeControl = new ShadeControl(shadeChannel1,cloud);
        int shadeChannel2=1;
        hBox.getChildren().add(0,shadeControl.getView());
        Scene scene = new Scene(pane, 450, 450);
        primaryStage.setTitle("Domotic Devices Simulator");
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
