README:

Para el desarrollo de este proyecto de un sistema domótico se particionó el trabajo en cuatro módulos que poseen criterios de diseño especificados a continuación.
Este proyecto fue desarrollado en el entorno de IntelliJ y necesita añadir en la configuración la librería del conjunto de paquetes de Javafx versiónj 18.0.1
El compilado de archivos del proyecto cuenta con archivos MAKEFILE que permiten compilar, ejecutar y limpiar los archivos .class para facilitar este proceso.
Existe una carpeta de archivos por módulo.

Modulo 1: Este módulo muestra una representación gráfica de un dispositivo domótico de tipo LÁMPARA el cual es controlado a partir de la interacción con el usuario.
	  El dispositivo de tipo CONTROL DE LÁMPARA consta de un botón el cúal al ser presionado cambia el estado de la lámpara de encendendido a apagado y viceversa.
	  El encendido y apagado de la lámpara se ve reflejado en un cambio de color en la pantalla del dispositivo.
		- Lámpara Encendida -> Pantalla de color Blanca.
		- Lámpara Apagada   -> Pantalla de color Negra.

Modulo 2: Este módulo extiende la interfaz gráfica del módulo 1 y agrega en la parte superior una representación gráfica de un dispositivo domótico de tipo CORTINA 
	  el cual tiene enlazado un dispositivo domótico de tipo CONTROL DE CORTINA el cual se encarga de controlar la acciones a realizar por la cortina 
	  correspondiente al canal asociado y representado en el botón central del control.
	  El control de cortina consta de tres botones.
		- Botón superior para cerrar la cortina.
		- Botón central  para detener el movimiento de la cortina. 
		- Botón inferior para extender la cortina.

Modulo 3: Este módulo extiende la interfaz gráfica del módulo 2 y agrega un segundo dispositivo de tipo LÁMPARA y extiende las funcionalidades del dispositivo control
	  de lámpara, utilizando una objeto SPINNER que permite cambiar el canal de la lámpara a configurar a modo de manipular cada una independientemente y un objeto
	  SLIDER el cual permite cambiar el valor que configura las intensidades de colores RGB (RED, GREEN, BLUE). El cambio de colores se verá reflejado únicamente 
	  si la lámpara en cuestión se encuentra encendida.

Modulo 4: Este módulo extiende la interfaz gráfica del módulo 3 y agrega una segunda cortina y extiendes las funcionalidades del dispositivo control de cortinas agregando
	  nuevos botónes que permiten configurar el canal de la cortina que se quiere manipular. 
	  Además, como fue solicitado, en la región comprendida por las cortinas es posible visualizar videos que simulan vistas de Valparaíso tal como si fueran cortinas
	  reales. La cortina enlazada al canal 1 tiene un tamaño de proporciones 16:9 y la cortina enlazada al canal 2 tiene un tamaño de proporciones 1:1.
	  Finalmente se agregó la interacción con el usuario a través de el teclado para cortinas y lámparas. 

		- Mantener presionada tecla  UP  para cerrar   la cortina.
		- Mantener presionada tecla DOWN para extender la cortina.
		- Presionar tecla   P   para cambiar estado de  la lámpara. 
	  
	  Cabe destacar que al liberar las teclas UP y DOWN se detendra el movimiento de la cortina. 
	  En este módulo, al tener archivos de video, puede que al compilar no aparezcan, sin embargo estan y se han configurado previamente para que aparezcan en la escena,
	  en caso de no aparecer, le pedimos recompilar el proyecto.
	  Finalmente se debe tener en cuenta que para que no existan problemas al controlar ya sean las lámparas o las cortinas se debe seleccionar previamente el control del 
	  dispositivo deseado.



Se decide no optar a la bonificación. 

Curso: ELO329 Programación Orientada a Objetos.
Profesor: Werner Creixell.
Nombres de programadores: 	- Matias Valenzuela	rol: 201821015-3
				- Diego Sepulveda	rol: 201704156-0
				- Britney Sandon	rol: 201821060-9
				- Jorge Álvarez		rol: 201721065-6
Fecha: 11/06/2022.





