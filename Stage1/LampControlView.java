import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

public class LampControlView extends VBox {
    public LampControlView(LampControl lampControl) {
        this.lampControl = lampControl;
        Image img = new Image("rsc/powerImage.jpg");
        ImageView view = new ImageView(img);
        Button bt = new Button();
        bt.setPrefSize(57,57);
        bt.setGraphic(view);
        bt.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent press) {

                lampControl.pressPower();
            }
        });
        getChildren().addAll(bt);

    }
    private LampControl lampControl;
}
