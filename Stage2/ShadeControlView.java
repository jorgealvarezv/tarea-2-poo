import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;

public class ShadeControlView extends BorderPane {
    public ShadeControlView (ShadeControl sc){
        Image bUp = new Image("rsc/arriba.jpeg");
        Image bDown = new Image("rsc/abajo.jpeg");
        ImageView viewUp = new ImageView(bUp);
        ImageView viewDown = new ImageView(bDown);
        viewUp.setFitHeight(20);
        viewUp.setFitWidth(20);
        viewDown.setFitHeight(20);
        viewDown.setFitWidth(20);
        ShadeControl shadeControl = sc;
        Button channelButton = new Button(""+sc.getChannel());

        channelButton.setOnAction( e-> {
            sc.stop();
        });
        Button upButton = new Button();
        //upButton.setPrefSize(10,10);
        upButton.setGraphic(viewUp);
        upButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                sc.startUp();
            }
        });

        Button downButton = new Button();
        //downButton.setPrefSize(10,10);
        downButton.setGraphic(viewDown);
        downButton.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                sc.startDown();
            }
        });
        setCenter(channelButton);
        setTop(upButton);
        setBottom(downButton);
    }
}
