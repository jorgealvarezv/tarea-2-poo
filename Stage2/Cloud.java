import java.util.ArrayList;

public class Cloud {
    public Cloud() {
        lamps = new ArrayList<DomoticDevice>();
        rollerShades = new ArrayList<DomoticDevice>();
    }
    public void addLamp(Lamp l){
        lamps.add(l);
    }
    public void addRollerShade(RollerShade roller){
        rollerShades.add(roller);
    }

    private ArrayList<DomoticDevice> getDomoticDeviceAtChannel( ArrayList<DomoticDevice> devices, int channel){
        ArrayList<DomoticDevice> domos = new ArrayList<DomoticDevice>(devices.size());
        for (DomoticDevice disp: devices) {
            if(channel == disp.getChannel()){
                domos.add(disp);
            }

        }
        return domos;
    }

    public void changeLampPowerState(int channel){
        ArrayList<DomoticDevice> dd = getDomoticDeviceAtChannel(lamps, channel);
        for(DomoticDevice l:dd){
            Lamp lp = (Lamp) l;
            lp.changePowerState();
        }
    }

    public void startShadeUp(int channel){
        ArrayList<DomoticDevice> dd = getDomoticDeviceAtChannel(rollerShades, channel);
        for (DomoticDevice roll: dd
        ) {
            RollerShade roller = (RollerShade) roll;
            roller.startUp();

        }
    }
    public void startShadeDown(int channel){
        ArrayList<DomoticDevice> dd = getDomoticDeviceAtChannel(rollerShades, channel);
        for (DomoticDevice roll: dd
        ) {
            RollerShade roller = (RollerShade) roll;
            roller.startDown();

        }
    }
    public void stopShade(int channel){
        ArrayList<DomoticDevice> dd = getDomoticDeviceAtChannel(rollerShades, channel);
        for (DomoticDevice roll: dd
        ) {
            RollerShade roller = (RollerShade) roll;
            roller.stop();

        }
    }

    private ArrayList<DomoticDevice> lamps;
    private ArrayList<DomoticDevice> rollerShades;
}

//OLA3
